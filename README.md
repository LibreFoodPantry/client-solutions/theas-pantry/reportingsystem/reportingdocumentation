This project provides a user interface to create and store reports to send to the Worcester County Food Bank.

## General

Contains a description of the format for reports.

## GenerateWCFBReportFrontend

Frontend for sending reports.

## ReportingAPI

OpenAPI specification for the reporting system.

## ReportingBackend

Backend for managing reports.


## User story

A Pantry Administrator Requests a Monthly Report for the Worcester County Food Bank

The system verifies that the logged-in user is an Adminstrator.
The user specifies the month and year for the report.
The system generates the report as a downloadable Excel file that the administrator can forward to the Worcester County Food Bank.


Note: See [CustomerDocuments/November2020TheasPantryReport.pdf] (https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/documentation/-/blob/main/CustomerDocuments/November2020TheasPantryReport.pdf) for format.

## Architecture

* ReportingSystem
  * GenerateWCFBReport (feature) (Generate Worcester County Food Bank Report) which uses:
    * GenerateWCFBReportFrontend (component)
    * ReportingBackend (component)
  * ReportingBackend (component) which uses:
    * GuestInfoEvents (queue)
    * InventoryEvents (queue)
  * ReportingBackend


  ### ReportingSystem Standalone Integration Test

```plantuml
@startuml

skinparam ranksep 150

Actor Admin

node "AdminBrowser" {
  [Login] as AdminLogin
  [GenerateWCFBReportFrontend] as BrowserGenerateReport
}

package "ReportingSystem" {
  node NGINX <<Server>> {
    [GenerateWCFBReportFrontend]
  }

  node RabbitMQ <<Server>> {
    queue InventoryEvents
    queue GuestInfoEvents
  }

  node MongoDB <<Server>> {
    database ReportingDb
  }

  node Express.js <<Server>> {
    [ReportingBackend]
  }
  ReportingBackend --> InventoryEvents : backend-events-network
  ReportingBackend --> GuestInfoEvents : backend-events-network
  ReportingBackend --> ReportingDb : backend-db-network

  node KeyCloak <<Server>> {
    [Login]
  }
}

Admin -> AdminBrowser
Login .up.> AdminLogin
AdminLogin --> Login
GenerateWCFBReportFrontend .up.> BrowserGenerateReport
BrowserGenerateReport --> ReportingBackend
AdminLogin <- BrowserGenerateReport

@enduml
```
## Technology

## Decided

* REST API Specification: OpenAPI v3.0.3

* Frontend: Vue.js

* Backend: Express

* Data Persistence: MongoDB

* Containerization: Docker

* Container Orchestration: Kubernetes

* Version Control: Git

* DevOps Platform: GitLab

## Evaluating

* Message Queuing/Events Service: [RabbitMQ] (https://www.rabbitmq.com/)

* Identity and Access Management: [Keycloak] (https://www.keycloak.org/)







